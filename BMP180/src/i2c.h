#ifndef I2C_H_
#define I2C_H_

#include <stdint.h>

void i2c_init(void);
void I2C0_IRQHandler(void);
int8_t lecturaPendiente(uint8_t * buffer, uint8_t indice);
uint8_t i2c_leer(uint8_t * buffer, uint8_t cant);
void i2c_escribir(uint8_t dato);
void set_mode(uint8_t modo, uint8_t direccion);



#endif /* I2C_H_ */
