#include <cr_section_macros.h>
#include "swm.h"
#include "systick.h"
#include "funciones.h"
#include "i2c.h"
//pines----> SDA: PIO0_11, SCL: PIO0_10

int main(void) {
	i2c_init();
	systick_init();
    while(1){
    	Calibrar();
    	ObtenerDato();
    	CalcularDato();

    }
    return 0 ;
}
