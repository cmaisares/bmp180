#include "i2c.h"

#include "swm.h"


#define		SYSCON_SYSAHBCLKCTRL0		(*((volatile uint32_t *)(0x40048080)))

#define		SYSCON_PRESETCTRL0			(*((volatile uint32_t *)(0x40048088)))

#define		SYSCON_I2C0CLKSEL			(*((volatile uint32_t *)(0x400480A4)))

#define		I2C0_CFG					(*((volatile uint32_t *)(0x40050000)))
#define		I2C0_STAT					(*((volatile uint32_t *)(0x40050004)))
#define		I2C0_CLKDIV					(*((volatile uint32_t *)(0x40050014)))
#define		I2C0_MSTCTL					(*((volatile uint32_t *)(0x40050020)))
#define		I2C0_MSTTIME				(*((volatile uint32_t *)(0x40050024)))
#define		I2C0_MSTDATA				(*((volatile uint32_t *)(0x40050028)))

#define		INTENSET_I2C0				(*((volatile uint32_t *)( 0x40050008)))
#define		INTENCLR					(*((volatile uint32_t *)( 0x4005000C)))


#define		NVIC_ISER0						(*((volatile uint32_t *)(0xE000E100)))
#define		NVIC_ICER0						(*((volatile uint32_t *)(0xE000E180)))
#define		NVIC_ISER0_I2C0_MASK			(1 << 8)
#define		NVIC_ICER0_I2C0_MASK			(1 << 8)

#define RESTART 0
#define STOP 1

uint8_t pendiente = 0;

void i2c_init(void) {
	// Limpiamos reset del periferico
	SYSCON_PRESETCTRL0 &= ~(1 << 5);
	SYSCON_PRESETCTRL0 |= (1 << 5);

	// Seleccionamos FRO como fuente de clock del periferico
	SYSCON_I2C0CLKSEL = 0;

	// Habilitamos clock para el perfierico a nivel SYSCON
	SYSCON_SYSAHBCLKCTRL0 |= (1 << 5);

	// Configuramos pines en la SWM
	swm_power_on();
	swm_enable_i2c0_scl();
	swm_enable_i2c0_sda();

	// Configuramos clkdiv del periferico en /12
	I2C0_CLKDIV = 11;

	// Configuramos tiempo alto y bajo de SCL en 3
	I2C0_MSTTIME = 0x03 | (0x03 << 4);

	// Configuramos el periferico como modo master
	I2C0_CFG |= (1 << 0);

	NVIC_ISER0 = NVIC_ISER0_I2C0_MASK;

}

void i2c_escribir(uint8_t dato) {
	// Escribimos en data el dato a mandar
	I2C0_MSTDATA = dato;
	// Forzamos a que el periferico envie el dato en MSTDATA
	I2C0_MSTCTL = (1 << 0);
	// Enviamos STOP
	I2C0_MSTCTL = (1 << 2);

}

int8_t lecturaPendiente(uint8_t * buffer, uint8_t indice){
	int8_t retorno = 0;
	uint32_t aux = NVIC_ISER0;;
	NVIC_ICER0 |= NVIC_ICER0_I2C0_MASK;
	if (pendiente) {
		 buffer[indice] = I2C0_MSTDATA;
		 retorno = 1;
	}
	else{
		retorno = -1;
	}
	NVIC_ISER0 |= aux | NVIC_ICER0_I2C0_MASK;
	I2C0_MSTCTL = (1 << 2);
	return retorno;

}

void I2C0_IRQHandler(void){
	pendiente = 1;
	INTENCLR = (1 << 0);
}

void set_mode(uint8_t modo, uint8_t direccion){
	switch (modo) {
		case 0:
			I2C0_MSTDATA = (direccion << 1) | 0x00;
			break;
		case 1:
			I2C0_MSTDATA = (direccion << 1) | 0x01;
			break;
		default:
			break;
	}

	// Escribimos registro de control para iniciar la transmision
	// el periferico en este punto pasa a mandar el start + addr + r/w
	// Enviamos STOP
	I2C0_MSTCTL = (1 << 2);
}
