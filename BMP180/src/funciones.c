#include "funciones.h"
#include "i2c.h"
#include <stdint.h>
#include <cr_section_macros.h>

#define ADRESS_SENSOR 0x77

uint8_t calibrado, datoUTObtenido, datoUPObtenido, datoCalculado;
uint8_t buffer_calibrado[22];
int16_t ac1, ac2, ac3, b1, b2, mb, mc, md;
uint16_t ac4, ac5, ac6;
uint32_t UT, UP, x1, x2, x3, b3, b5, b6, p, T , P;
char tempe[10], pre[10];
uint32_t b4, b7;

int32_t leer_UP();
int32_t leer_UT();
int CalculoTemperatura(uint32_t tval);
int32_t CalculoPresion(uint32_t pval);

void ObtenerDato(){
	if(calibrado != 1){
		if (datoUTObtenido != 1){
			UT = leer_UT();
		}
		if (datoUPObtenido != 1){
			UP = leer_UP();
		}
	}
}

void CalcularDato(){
	T = CalculoTemperatura(UT);
	P = CalculoPresion(UP);
	datoCalculado = 1;
}

void Calibrar(){
	static uint8_t seteado = 0;
	int8_t seguir;
	static int8_t index = 0;
	if(calibrado != 1){
		if(index <22){
			if(seteado == 0){
				set_mode(1, 0xAA);
				seteado = 1;
			}
			seguir = lecturaPendiente(buffer_calibrado, index);
			if(seguir != -1){
				index++;
			}
			switch(index){
			case -1:
				break;
			case 1:
				ac1 = buffer_calibrado[0] | buffer_calibrado[1];
				break;
			case 3:
				ac2 = buffer_calibrado[2] | buffer_calibrado[3];
				break;
			case 5:
				ac3 = buffer_calibrado[4] | buffer_calibrado[5];
				break;
			case 7:
				ac4 = buffer_calibrado[6] | buffer_calibrado[7];
				break;
			case 9:
				ac5 = buffer_calibrado[8] | buffer_calibrado[9];
				break;
			case 11:
				ac6 = buffer_calibrado[10] | buffer_calibrado[11];
				break;
			case 13:
				b1 = buffer_calibrado[12] | buffer_calibrado[13];
				break;
			case 15:
				b2 = buffer_calibrado[14] | buffer_calibrado[15];
				break;
			case 17:
				mb = buffer_calibrado[16] | buffer_calibrado[17];
				break;
			case 19:
				mc = buffer_calibrado[18] | buffer_calibrado[19];
				break;
			case 21:
				md = buffer_calibrado[20] | buffer_calibrado[21];
				calibrado = 1;
				break;
			}

		}
	}
}

int32_t leer_UT(){
	uint8_t buffer[2];
	uint8_t indice = 0, cont = 0;
	int8_t retorno;
	uint32_t result = 0;
	set_mode(0, 0xF4);//Escritura en el registro 0xF4
	i2c_escribir(0x2E);//Escribir 0x2E en el registro 0xF4
	//calculo 5ms en dos vueltas de systick
	if(cont >= 2 && datoUTObtenido != 1){
		switch (indice) {
			case 0:
				set_mode(1, 0xF6);//Lectura en 0xF6
				retorno = lecturaPendiente(buffer, 0);
				if(retorno == 1){
					indice++;
					retorno = 0;
				}
				break;
			case 1:
				set_mode(1, 0xF7);//Lectura en 0xF7
				retorno = lecturaPendiente(buffer, 1);
				if(retorno == 1){
					result = buffer[0]<<8 | buffer[1];
					datoUTObtenido = 1;
					indice++;
				}
			default:
				break;
		}
	}
	else{
		cont++;
	}
	return result;
}

int CalculoTemperatura(uint32_t tval){
	int result;
	x1 = ((UT-ac6)*ac5)>>15;
	x2 = ((long)mc<<11/(x1+md));
	b5 = x1 + x2;
	result = ((b5 + 8)>>4);
	return result;//hay que dividirlo en 10 luego
}

int32_t leer_UP(){
	uint16_t oss = 3;
	uint8_t buffer[3], indice=0, retorno;
	uint8_t cont = 0;
	uint32_t result = 0;
	set_mode(0, 0xF4);//Modo escritura en 0xF4
	i2c_escribir(0x34+(oss<<6));//Escribir en 0xF4
	if(cont <= 2 && datoUPObtenido != -1){//Calculo el tiempo en dos vueltas del systick
		switch (indice) {
			case 0:
				set_mode(1, 0xF6);//Modo lectura en 0xF6
				retorno = lecturaPendiente(buffer, indice);
				if(retorno == 1){
					indice++;
					retorno = 0;
				}
				break;
			case 1:
				set_mode(1, 0xF7);//Modo lectura en 0xF7
				retorno = lecturaPendiente(buffer, indice);
				if(retorno == 1){
					indice++;
					retorno = 0;
				}
				break;
			case 3:
				set_mode(1, 0xF8);//Modo lectura en 0xF8
				retorno = lecturaPendiente(buffer, indice);
				if(retorno == 1){
					indice++;
					retorno = 0;
					result = buffer[0]<<8 | buffer[1];
					result <<= oss;
					result |= buffer[2];
					datoUPObtenido = 1;
				}
				break;
			default:
				break;
		}
	}
	return result;
}

int32_t CalculoPresion(uint32_t pval){
	uint16_t oss = 3;
	b6 = b5 - 4000;
	x1 = (b6 * b6)>>12;
	x1 *= b2;
	x1 >>= 11;
	x2 = ac2 * b6;
	x2 >>= 11;
	x3 = x1 + x2;
	b3 = (((((long)ac1)* 4 + x3)<< oss)+ 2)>> 2;
	x1 = (ac3 * b6) >> 13;
	x2 = (b1 * ((b6 * b6) >> 12)) >> 16;
	x3 = ((x1 + x2)+2) >> 2;
	b4 = (ac4 * (unsigned long)(x3 + 32768)) >> 15;
	b7 = ((unsigned long)(pval - b3) * (50000 >> oss));
	p = b7 < 0x80000000 ? (b7 << 1)/b4 : (b7/b4) << 1;
	x1 = p >> 8;
	x1 *= x1;
	x1 = (x1 * 3038) >> 16;
	x2 = (p * -7357) >> 16;
	p += (x1 + x2 + 3791) >> 4;
	return p;
}
